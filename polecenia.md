# Git
cd ..
git clone https://bitbucket.org/ev45ive/sages-angular-adv-czerwiec.git sages-angular-adv-czerwiec
cd sages-angular-adv-czerwiec
npm i 
npm start

# update
git stash -u 
git pull


## VS CODE
https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode
https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype
# Instalacje
node -v 
v14.17.0

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1

code -v 
1.57.0

chrome://version

## Chrome
https://chrome.google.com/webstore/detail/angular-devtools/ienfalfjdbdpebioblfackkekamfmbnh
https://augury.rangle.io/

## UI Kits
https://ng.ant.design/components/table/en
https://www.primefaces.org/primeng/showcase/#/
https://ng-bootstrap.github.io/#/components/nav/overview

## Material
https://material.io/design/material-studies/crane.html#product-architecture
https://material.angular.io/components/categories
https://material.angular.io/cdk/categories


## NX workspaces 
https://nx.dev/latest/angular/getting-started/nx-setup

npx create-nx-workspace@latest --help

cd ..
npx create-nx-workspace@latest sages-angular-adv-czerwiec --preset=angular 
√ What to create in the new workspace · angular
√ Application name                    · sages-angular-adv
√ Default stylesheet format           · scss
√ Use Nx Cloud? (It's free and doesn't require registration.) · No
>  NX  Nx is creating your workspace.
✔ Installing dependencies with npm
✔ Nx has successfully created the workspace.

cd sages-angular-adv-czerwiec 
npm run nx --version
npx nx --version

## Nx CLI
npm i -g nx
nx --version

npm start -- -o

## Nx PLuginx
nx list

## Material
nx add @angular/material
ℹ Using package manager: npm
✔ Found compatible package version: @angular/material@12.0.4.
✔ Package information loaded.

The package @angular/material@12.0.4 will be installed and executed.
Would you like to proceed? Yes
✔ Package successfully installed.
? Choose a prebuilt theme name, or "custom" for a custom theme: Indigo/Pink        [ Preview: https://material.angular.io?theme=indigo-pink ]
? Set up global Angular Material typography styles? Yes 
? Set up browser animations for Angular Material? Yes
UPDATE package.json (2498 bytes)
✔ Packages installed successfully.
    Your project is not using the default builders for "test". This means that we cannot add the configured theme to the "test" target.
UPDATE apps/sages-angular-adv/src/app/app.module.ts (403 bytes)
UPDATE angular.json (4833 bytes)
UPDATE apps/sages-angular-adv/src/index.html (663 bytes)       
UPDATE apps/sages-angular-adv/src/styles.scss (181 bytes)    

https://material.angular.io/guide/theming

## Storybook
npm i @nrwl/storybook

ng generate @nrwl/storybook:configuration --name=sages-angular-adv --uiFramework=@storybook/angular --configureCypress

ng generate @schematics/angular:module --name=shared --module=app 
ng generate @angular/material:navigation --name=shared/navigation --module=shared --export

## Material theming
https://material.angular.io/guide/theming#using-a-pre-built-theme 
https://github.com/angular/components/blob/master/src/material/core/theming/prebuilt/indigo-pink.scss

ng generate @schematics/angular:module --name=playlists --module=app --route=playlists --routing 
ng generate @schematics/angular:component --name=playlists/components/playlist-list --style=scss --displayBlock
ng generate @schematics/angular:component --name=playlists/components/playlist-list-item --style=scss --displayBlock
ng generate @schematics/angular:component --name=playlists/components/playlist-details --style=scss --displayBlock
ng generate @schematics/angular:component --name=playlists/components/playlist-edit-form --style=scss --displayBlock

## Angular Flex Layout
https://github.com/angular/flex-layout/wiki/API-Documentation
npm i -s @angular/flex-layout @angular/cdk

## Storybook stories
ng generate @nrwl/angular:stories --name=sages-angular-adv
  
  No stories generated because there were no components declared in /apps/sages-angular-adv/src/app/playlists/playlists-routing.module.ts. Hint: you can always generate stories later with the 'nx generate @nrwl/angular:stories --name=sages-angular-adv' command

nx run  sages-angular-adv:storybook

## Story
```ts
import { PlaylistDetailsComponent } from './playlist-details.component';

export default {
  title: 'PlaylistDetailsComponent',
  component: PlaylistDetailsComponent
}

export const primary = () => ({
  moduleMetadata: {
    imports: []
  },
  props: {
  }
})
```

nx g module core -m app


## TypeScript strict mode
TsConfig -> compilerOptions -> strict (strictNullChecks, noImplicitAny)
angularCompilerOptions -> strictTemplates (ngIf..)
https://dev.to/briwa/how-strict-is-typescript-s-strict-mode-311a

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## COmponent communication

nx g c shared/experiments/tab-experiments
nx g c shared/experiments/tab-panels --export
nx g c shared/experiments/tab-panel --export

## Music Search
ng g m music-search -m=app --route=music --routing 
ng g c music-search/containers/album-search --style=scss --displayBlock
ng g c music-search/components/search-form --style=scss --displayBlock
ng g c music-search/components/search-results --style=scss --displayBlock
ng g c music-search/components/album-card --style=scss --displayBlock

## Services

nx g service core/services/music-search 

# RxJs
https://rxviz.com/ 
https://rxmarbles.com/#pluck
https://rxjs.dev/api/operators/map
http://reactivex.io/rxjs/identifiers.html 


## Form validation
https://ng.ant.design/components/form/en#components-form-demo-validate-template
https://material.angular.io/components/input/overview#changing-when-error-messages-are-shown

## NgRX
npm install --save @ngrx/store @ngrx/effects @ngrx/entity @ngrx/store-devtools  @ngrx/schematics 
ng add @ngrx/schematics@latest

ng generate @ngrx/schematics:store --name=State --module=app --project=sages-angular-adv --no-flat --root --stateInterface=AppState

ng generate @ngrx/schematics:effect --name=app --module=app --project=sages-angular-adv --api --group --root

ng generate @ngrx/schematics:feature --name=playlists --project=sages-angular-adv --module=playlists --api --group --reducers=reducers/index.ts

## Counter
ng generate @ngrx/schematics:action --name=counter --project=sages-angular-adv --group
ng generate @ngrx/schematics:reducer --name=counters --project=sages-angular-adv --module=app --no-flat --group --reducers=reducers/index.ts

ng generate @ngrx/schematics:selector --name=app --project=sages-angular-adv --group

ng generate @ngrx/schematics:container --name=shared/containers/counter --project=sages-angular-adv --style=scss --changeDetection=OnPush --export


## Mobix, immer, itp
https://immerjs.github.io/immer/
https://github.com/timdeschryver/ngrx-immer
