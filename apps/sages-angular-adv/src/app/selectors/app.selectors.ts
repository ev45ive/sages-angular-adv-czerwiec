import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../reducers';

export const counterValueSelector = createSelector(
    (state: AppState) => state.counters,
    (counters) => counters.counter.value)