import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromPlaylists from '../reducers/playlists.reducer';

export const selectPlaylistsState = createFeatureSelector<fromPlaylists.State>(
  fromPlaylists.playlistsFeatureKey
);

export const selectPlaylists = createSelector(
  selectPlaylistsState, state => state.items
)


export const selectPlaylistsMode = createSelector(
  selectPlaylistsState, state => state.mode
)

export const selectSelectedPlaylist = createSelector(
  selectPlaylistsState,
  state => state.items.find(p => p.id === state.selectedId)
)