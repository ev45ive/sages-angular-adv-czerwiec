import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { scan } from 'rxjs/operators';
import { MusicSearchService } from '../../../core/services/music-search/music-search.service';

@Component({
  selector: 'app-recent-searches',
  templateUrl: './recent-searches.component.html',
  styleUrls: ['./recent-searches.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecentSearchesComponent implements OnInit {

  searches = this.service.queryChange.pipe(
    scan((queryList, query) => {
      queryList.push(query)
      return queryList.slice(-5)
    }, [] as string[])
  )

  constructor(
    private service: MusicSearchService
  ) {
  }

  search(query: string) {
    this.service.searchAlbums(query)
  }

  ngOnInit(): void {
  }

}
