import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { decCounter, incCounter, resetCounter } from '../../../actions/counter.actions';
import { AppState } from '../../../reducers';
import { counterValueSelector } from '../../../selectors/app.selectors';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterComponent implements OnInit {

  counter$ = this.store.select(counterValueSelector)

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
  }

  inc() {
    this.store.dispatch(incCounter({ amount: 1 }))
  }

  dec() {
    this.store.dispatch(decCounter({ amount: 1 }))
  }

  reset() {
    this.store.dispatch(resetCounter())
  }

}
