import { AfterContentInit, Input, IterableDiffer, IterableDiffers, QueryList } from '@angular/core';
import { Component, ContentChild, ContentChildren, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { TabPanelComponent } from '../tab-panel/tab-panel.component';

@Component({
  selector: 'app-tab-panels',
  templateUrl: './tab-panels.component.html',
  styleUrls: ['./tab-panels.component.scss']
})
export class TabPanelsComponent implements OnInit, AfterContentInit {
  @Input() accordion?: string;

  children: TabPanelComponent[] = []

  register(child: TabPanelComponent) {
    this.children.push(child)
  }

  toggle(child: TabPanelComponent) {
    this.children.forEach(tab => {
      tab.open = false;
      if (tab == child) {
        child.open = !child.open
      }
    })
  }

  unregister(child: TabPanelComponent) {
    this.children = this.children.filter(c => c !== child)
  }

  ngOnInit(): void {

  }

  ngAfterContentInit(): void {

  }

  // @ContentChildren(TabPanelComponent) panels = new QueryList<TabPanelComponent>()

  // private differ: IterableDiffer<TabPanelComponent>

  // constructor(private differs: IterableDiffers) {
  //   this.differ = this.differs.find(this.panels).create();

  // }

  // subsMap = new Map<TabPanelComponent, Subscription>()

  // updateChildren() {
  //   const diff = this.differ.diff(this.panels)!;

  //   diff.forEachAddedItem(record => {
  //     console.log('forEachAddedItem', record)
  //     const sub = record.item.toggled.subscribe(open => {
  //       this.panels.forEach(tab => {
  //         if (this.accordion === undefined) { return }

  //         if (tab !== record.item)
  //           tab.open = false
  //       })
  //     })
  //     this.subsMap.set(record.item, sub)
  //   })

  //   diff.forEachRemovedItem(record => {
  //     console.log('forEachRemovedItem', record)

  //     const sub = this.subsMap.get(record.item)
  //     sub?.unsubscribe()
  //     this.subsMap.delete(record.item)
  //   })
  // }

  // ngAfterContentInit(): void {
  //   // this.panels.forEach(activeTab => {
  //   //   activeTab.toggled.subscribe(open => {
  //   //     this.panels.forEach(tab => {
  //   //       if (this.accordion === undefined) { return }

  //   //       if (tab !== activeTab)
  //   //         tab.open = false
  //   //     })
  //   //   })
  //   // })
  //   this.updateChildren()


  //   this.panels.changes.subscribe(change => {
  //     // console.log('change', change, change.toArray(), this.children)

  //     this.updateChildren()
  //   })
  // }

  // ngAfterViewInit(): void {
  //   //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
  //   //Add 'implements AfterViewInit' to the class.
  // }



}
