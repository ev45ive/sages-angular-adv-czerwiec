import { NgZone } from '@angular/core';
import { ApplicationRef, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClockComponent implements OnInit {
  time = ' --:-- '

  constructor(
    private ngZone: NgZone,
    private cdr: ChangeDetectorRef,
    private app: ApplicationRef) {

    // check whole application
    // app.tick()

    // Never check me unless I run detectChanges
    // cdr.detach()
    
    // Too soon - not mounted yet
    // this.cdr.detectChanges() // ASSERTION ERROR: Should be run in update mode 
  }

  ngOnInit(): void {
    // this.cdr.detectChanges()

    // Dont check other components
    this.ngZone.runOutsideAngular(()=>{

      setInterval(() => {
        this.time = (new Date()).toLocaleTimeString()
        
        // Check everything
        this.cdr.detectChanges()

        // Check me also with next parent check
        // this.cdr.markForCheck()
      }, 1000)
    })

  }

  // i = 0;

  // log(){
  //   this.title = 'Changed title!' + this.i++
  //   console.log('Detect Changes')

  //   // core.js:6456 ERROR Error: 
  //   // NG0100: ExpressionChangedAfterItHasBeenCheckedError: 
  //   // Expression has changed after it was checked. 
  //   // Previous value: 'Clock'. Current value: 'Changed title!'.. 
  //   // Find more at https://angular.io/errors/NG0100
  // }
}
