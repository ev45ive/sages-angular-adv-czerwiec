import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab-experiments',
  templateUrl: './tab-experiments.component.html',
  styleUrls: ['./tab-experiments.component.scss']
})
export class TabExperimentsComponent implements OnInit {
  panels = ['X', 'Y', 'Z']

  addPanel() {
    this.panels.push(Math.ceil(Math.random() * 10).toString())
  }

  removePanel(p:string) {
    this.panels = this.panels.filter(x => x !== p)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
