import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabExperimentsComponent } from './tab-experiments.component';

describe('TabExperimentsComponent', () => {
  let component: TabExperimentsComponent;
  let fixture: ComponentFixture<TabExperimentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabExperimentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabExperimentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
