import { Component, EventEmitter, OnInit, Optional, Output } from '@angular/core';
import { TabPanelsComponent } from '../tab-panels/tab-panels.component';

@Component({
  selector: 'app-tab-panel',
  templateUrl: './tab-panel.component.html',
  styleUrls: ['./tab-panel.component.scss']
})
export class TabPanelComponent implements OnInit {

  open = false;

  @Output() toggled = new EventEmitter<this['open']>();

  toggle() {

    if (!this.parent) {
      this.open = !this.open
    } else {
      this.parent?.toggle(this)
    }

    this.toggled.emit(this.open)
  }

  constructor(
    @Optional() private parent: TabPanelsComponent | null
  ) {
    this.parent?.register(this)
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.parent?.unregister(this)
  }

}
