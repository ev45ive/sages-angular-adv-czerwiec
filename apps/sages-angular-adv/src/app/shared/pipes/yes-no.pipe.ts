import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yesno'
})
export class YesNoPipe implements PipeTransform {

  transform(value: boolean, yes = 'Yes', no = 'No'): string {
    return value ? yes : no
  }

}
