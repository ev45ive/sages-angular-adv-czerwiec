import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { YesNoPipe } from './pipes/yes-no.pipe';
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ClockComponent } from './experiments/clock/clock.component';
import { CardComponent } from './components/card/card.component';
import { MatCardModule } from '@angular/material/card';
import { TabExperimentsComponent } from './experiments/tab-experiments/tab-experiments.component';
import { TabPanelsComponent } from './experiments/tab-panels/tab-panels.component';
import { TabPanelComponent } from './experiments/tab-panel/tab-panel.component';
import { TabPanelHeaderComponent } from './experiments/tab-panel-header/tab-panel-header.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { RecentSearchesComponent } from './containers/recent-searches/recent-searches.component';
import { CounterComponent } from './containers/counter/counter.component';
@NgModule({
  declarations: [
    NavigationComponent,
    YesNoPipe,
    ClockComponent,
    CardComponent,
    TabExperimentsComponent,
    TabPanelsComponent,
    TabPanelComponent,
    TabPanelHeaderComponent,
    RecentSearchesComponent,
    CounterComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CommonModule,
    LayoutModule,
    MatSelectModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatCardModule,
    MatSnackBarModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    NavigationComponent,
    FlexLayoutModule,

    LayoutModule,
    MatSelectModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatSnackBarModule,

    /* Components */
    YesNoPipe,
    CardComponent,
    ClockComponent,
    TabPanelsComponent,
    TabPanelComponent,
    TabPanelHeaderComponent,
    RecentSearchesComponent,
    CounterComponent
  ]
})
export class SharedModule { }
