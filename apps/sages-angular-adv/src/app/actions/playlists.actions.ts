import { createAction, props } from '@ngrx/store';
import { Playlist } from '../core/model/Playlist';

export const loadPlaylists = createAction(
  '[Playlists] Load Playlists Start'
);

export const loadPlaylistsSucces = createAction(
  '[Playlists] Load Playlists Succes',
  props<{ data: Playlist[] }>()
);

export const loadPlaylistsFailure = createAction(
  '[Playlists] Load Playlists Failure',
  props<{ error: any }>()
);

export const selectPlaylist = createAction(
  '[Playlists] Select Playlist',
  props<{ id: Playlist['id'] }>()
);
