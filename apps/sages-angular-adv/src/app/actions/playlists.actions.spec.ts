import * as fromPlaylists from './playlists.actions';

describe('loadPlaylistss', () => {
  it('should return an action', () => {
    expect(fromPlaylists.loadPlaylistss().type).toBe('[Playlists] Load Playlistss');
  });
});
