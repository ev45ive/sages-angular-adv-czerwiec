import { createAction, props } from '@ngrx/store';

export const loadCounters = createAction(
  '[Counter] Load Counters'
);

export const incCounter = createAction(
  '[Counter] Increment', props<{
    amount: number
  }>()
);

export const decCounter = createAction(
  '[Counter] Decrement', props<{
    amount: number
  }>()
);

export const resetCounter = createAction(
  '[Counter] Reset'
);





