import { Action, createReducer, on } from '@ngrx/store';
import { decCounter, incCounter, resetCounter } from '../../actions/counter.actions';


export const countersFeatureKey = 'counters';

export interface CounterState {
  counter: {
    value: number
  }
}

export const initialState: CounterState = {
  counter: { value: 10 }
};


export const reducer = createReducer(
  initialState,
  on(incCounter, (state, action) => ({
    ...state, counter: {
      ...state.counter, value: state.counter.value + action.amount
    }
  })),
  on(decCounter, (state, action) => ({
    ...state, counter: {
      ...state.counter, value: state.counter.value - action.amount
    }
  })),
  on(resetCounter, (state, action) => ({
    ...state, counter: {
      ...state.counter, value: 0
    }
  }))
);

