import { Action, createReducer, on } from '@ngrx/store';
import * as PlaylistsActions from '../actions/playlists.actions';
import { Playlist } from '../core/model/Playlist';

export const playlistsFeatureKey = 'playlists';

export interface State {
  items: Playlist[],
  selectedId?: Playlist['id'],
  mode: 'details' | 'edit',
  loading: boolean,
  message: string
}

export const initialState: State = {
  items: [], loading: false, message: '', mode: 'details'
};


export const reducer = createReducer(
  initialState,

  on(PlaylistsActions.loadPlaylists, state => ({
    ...state, items: [], loading: true, message: ''
  })),
  on(PlaylistsActions.loadPlaylistsSucces, (state, action) => ({
    ...state, loading: false, items: action.data
  })),
  on(PlaylistsActions.loadPlaylistsFailure, (state, action) => ({
    ...state, message: action.error?.message || 'Unexpected error'
  })),
  on(PlaylistsActions.selectPlaylist, (state, action) => ({
    ...state, selectedId: action.id
  }))

);

