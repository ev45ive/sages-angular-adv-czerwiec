import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromPlaylists from './playlists.reducer';
import * as fromCounters from '../reducers/counters/counters.reducer';


export interface AppState {
  [fromCounters.countersFeatureKey]: fromCounters.CounterState;

  [fromPlaylists.playlistsFeatureKey]?: fromPlaylists.State;
}

export const reducers: ActionReducerMap<AppState> = {

  // [fromPlaylists.playlistsFeatureKey]: fromPlaylists.reducer,
  [fromCounters.countersFeatureKey]: fromCounters.reducer,
};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
