import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Album } from '../core/model/Search';
import { INITIAL_SEARCH_RESULTS } from '../core/tokens';

const mockAlbums = [
  { id: '123', name: 'Album 123', type: 'album', images: [{ url: 'https://www.placecage.com/c/300/300' }] },
  { id: '234', name: 'Album 234', type: 'album', images: [{ url: 'https://www.placecage.com/c/400/400' }] },
  { id: '345', name: 'Album 345', type: 'album', images: [{ url: 'https://www.placecage.com/c/500/500' }] },
  { id: '456', name: 'Album 456', type: 'album', images: [{ url: 'https://www.placecage.com/c/600/600' }] },
] as Album[]


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    { provide: INITIAL_SEARCH_RESULTS, useValue: mockAlbums },
  ]
})
export class MocksModule { }
