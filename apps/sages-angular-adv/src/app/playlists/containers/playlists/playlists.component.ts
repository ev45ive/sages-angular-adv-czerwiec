import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { loadPlaylists, loadPlaylistsSucces, selectPlaylist } from '../../../actions/playlists.actions';
import { Playlist } from '../../../core/model/Playlist';
import * as fromStore from '../../../reducers';
import { selectPlaylists, selectPlaylistsMode, selectSelectedPlaylist } from '../../../selectors/playlists.selectors';


const playlistsData: Playlist[] = [{
  id: '123',
  name: 'Playlist Parent 123',
  public: false,
  description: 'my fav playlist 123'
}, {
  id: '234',
  name: 'Playlist 234',
  public: true,
  description: 'my fav playlist 234'
}, {
  id: '345',
  name: 'Playlist 345',
  public: false,
  description: 'my fav playlist 345'
}]

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.css']
})
export class PlaylistsComponent implements OnInit {

  playlists$ = this.store.select(selectPlaylists)
  playlist$ = this.store.select(selectSelectedPlaylist)
  mode$ = this.store.select(selectPlaylistsMode)

  constructor(private store: Store) { }

  ngOnInit(): void {
    // this.store.dispatch(loadPlaylistsSucces({ data: playlistsData }))
    this.store.dispatch(loadPlaylists())
  }

  selectPlaylistById(id: Playlist['id']) {
    this.store.dispatch(selectPlaylist({ id }))
  }
  editPlaylist(id: Playlist['id']) { }
  showPlaylist(id: Playlist['id']) { }
  savePlaylist(draft: Playlist) { }
}
