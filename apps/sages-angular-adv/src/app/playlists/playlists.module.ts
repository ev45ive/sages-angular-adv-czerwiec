import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';
import { PlaylistListItemComponent } from './components/playlist-list-item/playlist-list-item.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistEditFormComponent } from './components/playlist-edit-form/playlist-edit-form.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import * as fromPlaylists from '../reducers/playlists.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PlaylistsEffects } from '../effects/playlists.effects';
import { PlaylistsComponent } from './containers/playlists/playlists.component';

const routes: Routes = [
  // { path: '', component: PlaylistsComponent }
  { path: '', component: PlaylistsComponent }
];

@NgModule({
  declarations: [
    PlaylistsComponent,
    PlaylistListComponent,
    PlaylistListItemComponent,
    PlaylistDetailsComponent,
    PlaylistEditFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PlaylistsRoutingModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature(fromPlaylists.playlistsFeatureKey, fromPlaylists.reducer),
    EffectsModule.forFeature([PlaylistsEffects])
  ]
})
export class PlaylistsModule { }
