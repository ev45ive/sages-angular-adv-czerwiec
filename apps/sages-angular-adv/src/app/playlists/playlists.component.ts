import { Component, OnInit } from '@angular/core';
import { Playlist } from '../core/model/Playlist';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss']
})
export class PlaylistsComponent2 implements OnInit {

  mode: 'details' | 'edit' = 'details'

  playlists: Playlist[] = [{
    id: '123',
    name: 'Playlist Parent 123',
    public: false,
    description: 'my fav playlist 123'
  }, {
    id: '234',
    name: 'Playlist 234',
    public: true,
    description: 'my fav playlist 234'
  }, {
    id: '345',
    name: 'Playlist 345',
    public: false,
    description: 'my fav playlist 345'
  }]

  selectedId?= '234'
  selectedPlaylist?: Playlist

  constructor() { }

  selectPlaylistById(playlistId?: Playlist['id']) {
    this.selectedId = playlistId
    this.selectedPlaylist = this.playlists.find(p => p.id === playlistId);
  }

  editPlaylist(playlistId: Playlist['id']) {
    this.selectPlaylistById(playlistId)
    this.mode = 'edit'
  }

  savePlaylist(playlist: Playlist) {
    this.mode = 'details'
  }

  showPlaylist(playlistId: Playlist['id']) {
    this.selectPlaylistById(playlistId)
    this.mode = 'details'
  }

  ngOnInit(): void {
    this.selectPlaylistById(this.selectedId)
  }

}
