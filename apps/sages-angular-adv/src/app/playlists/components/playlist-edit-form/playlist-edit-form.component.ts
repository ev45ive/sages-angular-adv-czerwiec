import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

@Component({
  selector: 'app-playlist-edit-form',
  templateUrl: './playlist-edit-form.component.html',
  styleUrls: ['./playlist-edit-form.component.scss']
})
export class PlaylistEditFormComponent implements OnInit {

  @Input() playlist!: Playlist

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  cancelClick() {
    this.cancel.emit()
  }

  saveClick() {
    this.save.emit(this.playlist)
  }

  constructor() { }

  ngOnInit(): void {
    if (!this.playlist) {
      throw new Error('[playlist] input missing')
    }
  }

}
