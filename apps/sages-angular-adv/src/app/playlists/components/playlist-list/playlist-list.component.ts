import { NgForOf, NgForOfContext } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

NgForOf
NgForOfContext

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistListComponent implements OnInit {

  @Input('items') playlists: Playlist[] = []

  @Output() selectedIdChange = new EventEmitter<Playlist['id']>();

  @Input() selectedId? = ''

  select(playlistId: string) {
    // this.selectedId = playlistId
    this.selectedIdChange.emit(playlistId)
  }

  constructor() { }

  ngOnInit(): void {

  }

}
