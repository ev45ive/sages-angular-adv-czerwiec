import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { MusicSearchComponent } from './music-search.component';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { SharedModule } from '../shared/shared.module';
import { MocksModule } from '../mocks/mocks.module';
import { environment } from '../../environments/environment';

const routes: Routes = [
  {
    path: '', component: MusicSearchComponent,
    children: [
      // { path: '', pathMatch: 'full', redirectTo: '/music/search' },
      { path: 'search', component: AlbumSearchComponent }
    ]
  },
];

@NgModule({
  declarations: [
    MusicSearchComponent,
    AlbumSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MusicSearchRoutingModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    
  ]
})
export class MusicSearchModule { }
