import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { filter, map, share } from 'rxjs/operators';
import { MusicSearchService } from '../../../core/services/music-search/music-search.service';

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.scss']
})
export class AlbumSearchComponent implements OnInit {

  query = this.service.queryChange

  results = this.service.resultsChange.pipe(
    share()
  )

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) { }

  search(query: string) {
    // this.service.searchAlbums(query)

    this.router.navigate(['/music/search'], {
      queryParams: {
        q: query
      }
    })
  }

  ngOnInit(): void {
    // this.route.snapshot.queryParamMap.get('q')

    this.route.queryParamMap.pipe(
      map(params => params.get('q')),
      // map(q => q === null ? '' : q)
      filter((q: any): q is string => q !== null)
    ).subscribe(q => {
      this.service.searchAlbums(q)
    })
  }


}
