import { Component, Input, OnInit } from '@angular/core';
import { Album } from '../../../core/model/Search';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss']
})
export class AlbumCardComponent implements OnInit {

  @Input()  album!: Album

  constructor() { }

  ngOnInit(): void {
    if(!this.album){ throw new Error('Missing [album]')}
  }

}
