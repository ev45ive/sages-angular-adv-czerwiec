import { NgIf } from '@angular/common';
import { NgForOf } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators, ValidatorFn, AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, withLatestFrom } from 'rxjs/operators';

NgForOf
NgIf

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  errorMatcher = new MyErrorStateMatcher()

  ngOnChanges(changes: SimpleChanges): void {
    (this.searchForm
      .get('query') as FormControl)
      .setValue(changes['query'].currentValue, {
        emitEvent: false
      })
  }


  @Input() query: string | null = ''

  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    const badword = 'batman';
    const hasError = String(control.value).includes(badword)

    return hasError ? { 'censor': { badword } } : null
  }

  censorAsync: AsyncValidatorFn = (control: AbstractControl): Observable<ValidationErrors | null> => {
    // return this.http.get('..',{}).pipe(map(res => res.error? { 'censor': { badword } } : null))
    // console.log('Create new Observable for new query')

    return new Observable((subscriber) => {
      // console.log('Subscribed')
      const handle = setTimeout(() => {
        const badword = 'batman';
        const hasError = String(control.value).includes(badword)
        subscriber.next(hasError ? { 'censor': { badword } } : null)
        // console.log('Next')

        subscriber.complete()
        // console.log('Complete')
        // subscriber.error
      }, 1000)
      return () => {
        // console.log('Unsubscribe')
        clearTimeout(handle)
      }
    })
    // .pipe().subscribe({ next() { }, error() { }, complete() { } }).unsubscribe()
  }

  searchForm = new FormGroup({
    query: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      // this.censor
      // Validators.pattern('\d+'),
    ], [
      this.censorAsync
    ]),
    extras: new FormGroup({
      type: new FormControl('album'),
    })
  })

  advancedOpen = false

  @Output() search = new EventEmitter<string>();

  constructor() {
    (window as any).form = this.searchForm;

    const statusChanges = this.searchForm.get('query')!.statusChanges
    const valueChanges = this.searchForm.get('query')!.valueChanges.pipe(
      // wait 300ms of silence
      debounceTime(300),
      map(String),
      // minimum 3 chars
      filter(query => query.length >= 3),
      // no duplicates
      distinctUntilChanged(),
    )

    const searchChange = statusChanges.pipe(
      filter<'VALID'>(status => status == 'VALID'),
      withLatestFrom(valueChanges),
      map(([_, value]) => value)
    )
    searchChange.subscribe(this.search)
  }


  submit() {
    if (this.searchForm.valid) {
      this.search.emit(this.searchForm.get('query')?.value)
    }
  }

  ngOnInit(): void {
  }

}
