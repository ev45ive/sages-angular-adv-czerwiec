import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Album } from '../../../core/model/Search';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsComponent implements OnInit {

  @Input() results: Album[] = []

  // ChangeDetectionStrategy.OnPush :: 
  
  // ngOnChanges(changes: SimpleChanges): void {
  //   if (
  //     changes['results'].previousValue !== changes['results'].currentValue)) {
  //     this.cdr.detectChanges()
  //   }
  // }
  // constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

}
