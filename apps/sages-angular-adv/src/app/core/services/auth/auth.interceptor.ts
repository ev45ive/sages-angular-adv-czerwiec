import { ErrorHandler, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private auth: AuthService,
    private errorHandler: ErrorHandler,
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler)
    : Observable<HttpEvent<unknown>> {

    if (/* not my request */ false) {
      return next.handle(request)
    }
    /* or: HttpContext
      https://netbasal.com/new-in-angular-v12-passing-context-to-http-interceptors-308a1ca2f3dd
    */

    const authReq = request/* .clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    }) */

    /* or 
      OAuthModule.forRoot({
        resourceServer:{
          sendAccessToken:true,
          allowedUrls:[environment.api_url]
        }
      }),
     */

    return next.handle(authReq).pipe(catchError(error => {
      this.errorHandler.handleError(error)

      // if(errro instanceof InvalidResponseError){}

      if (!(error instanceof HttpErrorResponse)) {
        return throwError(new Error('Unexpected error'))
      }

      if (error.status === 401) {
        setTimeout(() => { this.auth.login() }, 2000)
      }

      if (isSpotifyAPIErrorResponse(error.error)) {
        return throwError(new Error(error.error.error.message))
      }

      return throwError(new Error(error.message))
    }))
  }
}

type SpotifyAPIErrorResponse = {
  error: { message: string, status: number }
}

function isSpotifyAPIErrorResponse(error: any): error is SpotifyAPIErrorResponse {
  return 'string' === typeof error?.error?.message
}