import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc'
import { environment } from 'apps/sages-angular-adv/src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private oauthService: OAuthService) {
    this.oauthService.configure(environment.authConfig);

    this.init();
  }

  async init() {
    await this.oauthService.tryLoginImplicitFlow({
      // preventClearHashAfterLogin: true
    });

    if (!this.getToken()) {
      this.login();
    }
  }

  login() {
    this.oauthService.initLoginFlow()
  }

  logout() {
    this.oauthService.logOut()
  }

  getToken() {
    return this.oauthService.getAccessToken()
  }
}
