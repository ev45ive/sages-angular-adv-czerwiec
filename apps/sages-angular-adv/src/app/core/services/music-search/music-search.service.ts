import { Inject, Injectable } from '@angular/core';
import { Album, SearchResponse, validateSearchResponse } from '../../model/Search';
import { API_URL, INITIAL_SEARCH_RESULTS } from '../../tokens';
import { HttpClient } from '@angular/common/http'
import { catchError, map, switchMap } from 'rxjs/operators';
import { EMPTY, ReplaySubject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class MusicSearchService {

  constructor(
    private http: HttpClient,
    private snackbar: MatSnackBar,
    @Inject(API_URL) private api_url: string,
    @Inject(INITIAL_SEARCH_RESULTS) private initial: Album[],
  ) {
    (window as any).subject = this.results

    /* Effects */
    this.query.pipe(
      switchMap(query =>
        this.http.get<SearchResponse<Album>>(`${this.api_url}/search`, {
          params: { type: 'album', q: query }
        }).pipe(
          map(validateSearchResponse<Album>('albums')),
          map(resp => resp.albums.items),
          catchError(error => {
            this.snackbar.open(error.message)
            return EMPTY
          })
        )),
    ).subscribe({
      next: albums => this.results.next(albums),
    })
  }

  /* State */
  private query = new ReplaySubject<string>(5/* , 10_000 */)
  private results = new BehaviorSubject<Album[]>(this.initial)

  /* Selectors */
  readonly queryChange = this.query.asObservable()
  readonly resultsChange = this.results.asObservable()

  /* Actions */
  searchAlbums(query: string) {
    this.query.next(query)
  }
}
