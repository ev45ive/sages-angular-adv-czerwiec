import { InjectionToken } from '@angular/core';
import { Album } from './model/Search';

export const API_URL = new InjectionToken<string>('API_URL');
export const INITIAL_SEARCH_RESULTS = new InjectionToken<Album[]>('INITIAL_SEARCH_RESULTS');
