
const a1: Array<string> = ['123'/* , 123 // error */]
const a2: Array<string | number> = ['123', 123]

type ElementRef<T> = {
    nativeElement?: T
}
type maybeBUtton = ElementRef<HTMLButtonElement>
let button!: maybeBUtton

button.nativeElement?.click()

declare function parse<T>(raw: string): T
declare function serialize<T>(object: T): string

const result1 = parse('') as {}
const result2 = serialize('' as {})

// function identity(x: any) { return x }
// const res3 = identity(123)
// res3.ala.ma.kota()

function identity<T>(x: T): T { return x }
const res3 = identity(123)
// res3.ala.ma.kota() // Error
const res4 = identity('123')

function takeFirst<T>(arr: T[]): T {
    return arr[0]
}

const arr1 = takeFirst<string>(['123'])
const arr2 = takeFirst(['123'])
const arr3 = takeFirst(['123', 123])
const arr4: boolean = takeFirst([true, 1 < 2]) // hint: takeFirst<boolean>(arr: boolean[]): boolean


interface Point { x: number, y: number }
interface Vector { x: number, y: number, length: number }

let p: Point = { x: 123, y: 123 }
let v: Vector = { x: 123, y: 123, length: 234 }

p = v

// p.length // Exits in JS, but not acceptable in TS
// v = p // Error: Property 'length' is missing in type 'Point' but required in type 'Vector'

let maybeResult: string | undefined;

// maybeResult.toLocaleLowerCase() // Object is possibly 'undefined'
if (maybeResult !== undefined) { // not certain - string | undefined
    maybeResult.toLocaleLowerCase() // certain - string
} else {
    maybeResult
}
const val1 = maybeResult ? maybeResult.toUpperCase() : ''
const val2 = maybeResult?.toUpperCase()
const val3 = maybeResult?.toUpperCase() || ''
const val4 = maybeResult && maybeResult.toUpperCase()
const val5 = maybeResult?.toUpperCase() ?? ''


let serverResult!: string | number
serverResult.toString()
// serverResult.toFixed() //   Property 'toFixed' does not exist on type 'string'

type ResponseTypes = string | number /* | boolean */;

function validateResponse(serverResponse: ResponseTypes) {
    if (typeof serverResponse === 'string') {
        return serverResponse.toUpperCase()
    }

    if (typeof serverResponse === 'number') {
        return serverResponse.toExponential()
    }

    // // exhaustiveness check
    // const _imposible: never = serverResponse
    // throw new Error('Unexpected response ' + serverResponse)
    checkExhaustiveness(serverResponse) // never returns
}

function checkExhaustiveness(value: never): never {
    throw new Error('Unexpected response ' + value)
}

function getId<T extends { id: string }>(obj: T): T['id'] {
    return obj.id
}

// function getKey(obj, key) {
//     return obj[key]
// }

function getKey<T extends {}, K extends keyof T>(obj: T, key: K): T[K] {
    return obj[key]
}
getKey({ x: 1, y: 2 }, 'x')