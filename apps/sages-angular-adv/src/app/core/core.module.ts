import { Inject, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';

import { OAuthModule } from 'angular-oauth2-oidc'
import { AuthInterceptor } from './services/auth/auth.interceptor';
import { environment } from '../../environments/environment';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    OAuthModule.forRoot({
      resourceServer:{
        sendAccessToken:true,
        allowedUrls:[environment.api_url]
      }
    }),
    CommonModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },

  ]
})
export class CoreModule {
  constructor(@Inject(HTTP_INTERCEPTORS) int: HttpInterceptor[]) {
    console.log(int);

  }
}
