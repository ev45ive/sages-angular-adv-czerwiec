import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, switchMap } from 'rxjs/operators';
import { Observable, EMPTY, of } from 'rxjs';

import * as PlaylistsActions from '../actions/playlists.actions';
import { HttpClient } from '@angular/common/http';
import { Playlist } from '../core/model/Playlist';
import { PagingObject } from '../core/model/Search';



@Injectable()
export class PlaylistsEffects {

  loadPlaylistss$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PlaylistsActions.loadPlaylists),
      switchMap((action) => {
        return this.http.get<PagingObject<Playlist>>('https://api.spotify.com/v1/me/playlists').pipe(
          map(resp => PlaylistsActions.loadPlaylistsSucces({
            data: resp.items
          })),
          catchError(error => of(PlaylistsActions.loadPlaylistsFailure({ error })))
        )
      })
    );
  });



  constructor(private actions$: Actions,
    private http: HttpClient) { }

}
