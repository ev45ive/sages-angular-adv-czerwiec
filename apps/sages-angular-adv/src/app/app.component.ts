import { Component } from '@angular/core';

@Component({
  selector: 'sages-angular-adv-czerwiec-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'sages-angular-adv';
}
