import { DoBootstrap, Inject, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { CoreModule } from './core/core.module';
import { TabExperimentsComponent } from './shared/experiments/tab-experiments/tab-experiments.component';
import { environment } from '../environments/environment';
import { Album } from './core/model/Search';
import { MusicSearchModule } from './music-search/music-search.module';
import { API_URL, INITIAL_SEARCH_RESULTS } from './core/tokens';
import { MusicSearchService } from './core/services/music-search/music-search.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { AuthService } from './core/services/auth/auth.service';
import { ApplicationRef } from '@angular/core';
import { MocksModule } from './mocks/mocks.module';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './effects/app.effects';
import { CounterComponent } from './shared/containers/counter/counter.component';

export const routes: Routes = [
  {
    path: '', redirectTo: '/music/search', pathMatch: 'full'
  },
  {
    path: 'tabs', component: TabExperimentsComponent
  },
  {
    path: 'counter', component: CounterComponent
  },
  {
    path: 'playlists',
    loadChildren: () => import('./playlists/playlists.module').then(m => m.PlaylistsModule)
  },
  {
    path: 'music',
    loadChildren: () => import('./music-search/music-search.module').then(m => m.MusicSearchModule)
  },
  {
    path: '**', component: TabExperimentsComponent
  }
]

@NgModule({
  declarations: [AppComponent],
  imports: [
    CoreModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, {
      // useHash: true
    }),
    SharedModule,
    environment.production ? [
    ] : MocksModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([AppEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [
    { provide: API_URL, useValue: environment.api_url },
    // { provide: INITIAL_SEARCH_RESULTS, useValue: [] },
    // {
    //   // provide: 'MusicSearchService'
    //   // provide: new InjectionToken...
    //   provide: MusicSearchService,
    //   useFactory(api_url: string, initial: Album[]) {
    //     // step1, step2, step3... return complex object...
    //     return new MusicSearchService(api_url, initial)
    //   },
    //   deps: [API_URL, INITIAL_SEARCH_RESULTS]
    // }
    // {
    //   // provide: AbstractMusicSearchService, useClass: SpotifyMusicSearchService,
    //   provide: MusicSearchService, useClass: MusicSearchService,
    //   // deps: [API_URL, INITIAL_SEARCH_RESULTS] // use @Inject() decorator instead
    // }
    // MusicSearchService // Not Tree-Shaken (dead code removal)
    // { provide: SpecialCaseService, useExisting: NormalCaseService}
  ],
  bootstrap: [AppComponent], // or DoBootstrap
})
export class AppModule /* implements DoBootstrap */ {

  constructor(
    private app: ApplicationRef,
    private auth: AuthService
  ) {

  }

  // ngDoBootstrap(appRef: ApplicationRef): void {
  //   // fetch(/config.json).then(config => ...
  //     this.app.bootstrap(AppComponent)
  // }

}
